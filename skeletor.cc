/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#define FSM
#define STATE(x)      s_##x :
#define NEXTSTATE(x)  goto s_##x

using std::cin;
using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::stringstream;

void fail(const string msg) {
  cout << msg << endl;
}

void print_info() {
  cout << "Skeletor - generating x/html structure and meta tags" << endl;
  cout << "Copyright (c) 2009 Martin Kopta <martin@kopta.eu>" << endl;
  cout << "Licensed under MIT license." << endl;
  cout << "====================================================" << endl;
}

void ask(const string question, const string default_answer, string *output) {
  cout << question << "? [" << default_answer << "]: ";
  getline(cin, *output);
  if (cin.eof()) {
    cout << endl << "Aborted" << endl;
    exit(0);
  }
  if (output->empty()) *output = default_answer;
}

int main() {
  bool css = true;
  string answer;
  string encoding;
  string indent = "  ";
  string tag_end;
  stringstream output;
  stringstream tmp;
  print_info();

  FSM {
    STATE(XHTML_OR_HTML) {
      ask("html or xhtml", "html", &answer);
      if ((answer == "html") or (answer == "HTML")) {
        tag_end = ">";
        NEXTSTATE(HTML_VERSION);
      } else if ((answer == "xhtml") or (answer == "XHTML")) {
        tag_end = "/>";
        NEXTSTATE(XML_ENCODING);
      } else {
        fail("Bad answer");
        NEXTSTATE(XHTML_OR_HTML);
      }
    }

    STATE(XML_ENCODING) {
      ask("xml encoding", "utf-8", &answer);
      output << "<?xml version=\"1.0\" encoding=\"";
      output << answer << "\" ?>" << endl;
      encoding = answer;
      NEXTSTATE(XML_VERSION);
    }

    STATE(XML_VERSION) {
      tmp.clear();
      tmp << "[a] XHTML 1.0 Strict" << endl;
      tmp << "[b] XHTML 1.0 Transitional" << endl;
      tmp << "[c] XHTML 1.0 Frameset" << endl;
      tmp << "[d] XHTML 1.1" << endl;
      tmp << "[e] XHTML Basic 1.0" << endl;
      tmp << "[f] XHTML Basic 1.1" << endl;
      tmp << "Which version";
      ask(tmp.str(), "d", &answer);
      if (answer == "a") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"";
        output << " ";
        output << "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
        output << endl;
      } else if (answer == "b") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 ";
        output << "Transitional//EN";
        output << " ";
        output <<
          "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        output << endl;
      } else if (answer == "c") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 ";
        output << "Frameset//EN\"";
        output << " ";
        output << "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">";
        output << endl;
      } else if (answer == "d") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"";
        output << " ";
        output << "\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">";
        output << endl;
      } else if (answer == "e") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML Basic 1.0//EN\"";
        output << " ";
        output << "\"http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd\">";
        output << endl;
      } else if (answer == "f") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML Basic 1.1//EN\"";
        output << " ";

        output << "\"http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd\">";
        output << endl;
      } else {
        fail("Answer should be [a-f]");
        NEXTSTATE(XML_VERSION);
      }
      NEXTSTATE(HTML_TAG_XML_LANG);
    }

    STATE(HTML_TAG_XML_LANG) {
      ask("tag html xml:lang", "en", &answer);
      output << "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"";
      output << answer << "\" lang=\"";
      ask("tag html lang", "en", &answer);
      output << answer << "\">" << endl;
      NEXTSTATE(INDENT_STRING);
    }

    STATE(HTML_VERSION) {
      tmp.clear();
      tmp << "[a] HTML 4.01 Strict" << endl;
      tmp << "[b] HTML 4.01 Transitional" << endl;
      tmp << "[c] HTML 4.01 Frameset" << endl;
      tmp << "[d] HTML 3.2" << endl;
      tmp << "[e] HTML 2.0" << endl;
      tmp << "Which version";
      ask(tmp.str(), "a", &answer);
      if (answer == "a") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\"";
        output << " \"http://www.w3.org/TR/html4/strict.dtd\">";
        output << endl;
      } else if (answer == "b") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 ";
        output << "Transitional//EN\"";
        output << " \"http://www.w3.org/TR/html4/loose.dtd\">";
        output << endl;
      } else if (answer == "c") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 ";
        output << "Frameset//EN\"";
        output << " \"http://www.w3.org/TR/html4/frameset.dtd\">";
        output << endl;
      } else if (answer == "d") {
        output << "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">";
        output << endl;
        css = false;
      } else if (answer == "e") {
        output << "<!DOCTYPE html PUBLIC \"-//IETF//DTD HTML 2.0//EN\">";
        output << endl;
        css = false;
      } else {
        fail("Answer should be [a-e]");
        NEXTSTATE(HTML_VERSION);
      }
      output << "<html>" << endl;
      NEXTSTATE(INDENT_STRING);
    }

    STATE(INDENT_STRING) {
      ask("Indentation string", "  ", &answer);
      indent = answer;
      output << indent << "<head>" << endl;
      NEXTSTATE(PAGE_TYPE_AND_CHARSET);
    }

    STATE(PAGE_TYPE_AND_CHARSET) {
      ask("page type", "text/html", &answer);
      output << indent << indent;
      output << "<meta http-equiv=\"content-type\" content=\"";
      output << answer << "; charset=";
      if (tag_end == "/>") {
        answer = encoding;
      } else {
        ask("page character set", "utf-8", &answer);
      }
      output << answer << "\"" << tag_end << endl;
      NEXTSTATE(CONTENT_LANG);
    }

    STATE(CONTENT_LANG) {
      ask("content language", "en", &answer);
      output << indent << indent;
      output << "<meta http-equiv=\"content-language\" content=\"";
      output << answer << "\"" << tag_end << endl;
      NEXTSTATE(PAGE_TITLE);
    }

    STATE(PAGE_TITLE) {
      ask("page title", "New Page", &answer);
      output << indent << indent << "<title>" << endl;
      output << indent << indent << indent;
      output << answer << endl;
      output << indent << indent << "</title>" << endl;
      NEXTSTATE(AUTHOR);
    }

    STATE(AUTHOR) {
      ask("page author", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<meta name=\"author\" content=\"";
        output << answer << "\"" << tag_end << endl;
      }
      NEXTSTATE(OWNER);
    }

    STATE(OWNER) {
      ask("page owner", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<meta name=\"owner\" content=\"";
        output << answer << "\"" << tag_end << endl;
      }
      NEXTSTATE(DESIGNER);
    }

    STATE(DESIGNER) {
      ask("page designer", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<meta name=\"designer\" content=\"";
        output << answer << "\"" << tag_end << endl;
      }
      NEXTSTATE(COPYRIGHT);
    }

    STATE(COPYRIGHT) {
      ask("copyright", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<meta name=\"copyright\" content=\"";
        output << answer << "\"" << tag_end << endl;
      }
      NEXTSTATE(KEYWORDS);
    }

    STATE(KEYWORDS) {
      ask("page keywords", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<meta name=\"keywords\" content=\"";
        output << answer << "\"" << tag_end << endl;
      }
      NEXTSTATE(DESCRIPTION);
    }

    STATE(DESCRIPTION) {
      ask("page description", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<meta name=\"description\" content=\"";
        output << answer << "\"" << tag_end << endl;
      }
      NEXTSTATE(ROBOTS);
    }

    STATE(ROBOTS) {
      ask("robots", "ALL, FOLLOW", &answer);
      output << indent << indent;
      output << "<meta name=\"robots\" content=\"";
      output << answer << "\"" << tag_end << endl;
      NEXTSTATE(CACHE);
    }

    STATE(CACHE) {
      ask("cache", "no-cache", &answer);
      output << indent << indent;
      output << "<meta name=\"cache\" content=\"";
      output << answer << "\"" << tag_end << endl;
      NEXTSTATE(STYLESHEET);
    }

    STATE(STYLESHEET) {
      if (!css) NEXTSTATE(FAVICON);
      ask("CSS file location", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<link rel=\"stylesheet\" type=\"text/css\"";
        output << " href=\"" << answer << "\"" << tag_end << endl;
      }
      NEXTSTATE(FAVICON);
    }

    STATE(FAVICON) {
      ask("Favicon location", "", &answer);
      if (!answer.empty()) {
        output << indent << indent;
        output << "<link rel=\"icon\" href=\"" << answer;
        output << "\" type=\"image/x-icon\"" << tag_end << endl;
        output << indent << indent;
        output << "<link rel=\"shortcut icon\" href=\"" << answer;
        output << "\" type=\"image/x-icon\"" << tag_end << endl;
      }
      NEXTSTATE(END_HEAD_START_BODY);
    }

    STATE(END_HEAD_START_BODY) {
      output << indent << "</head>" << endl;
      output << indent << "<body>" << endl;
      NEXTSTATE(DIV_CONTENT);
    }

    STATE(DIV_CONTENT) {
      if (!css) NEXTSTATE(END_BODY_END_HTML);
      ask("Generate main content div", "yes", &answer);
      if ((answer == "Yes") or (answer == "YES") or (answer == "yes")
          or (answer == "y") or (answer == "Y")) {
        output << indent << indent;
        output << "<div id=\"content\">" << endl;
        output << endl;
        output << indent << indent;
        output << "</div>" << endl;
      }
      NEXTSTATE(END_BODY_END_HTML);
    }

    STATE(END_BODY_END_HTML) {
      output << indent << "</body>" << endl;
      output << "</html>" << endl;
      NEXTSTATE(WRITE_TO_FILE);
    }

    STATE(WRITE_TO_FILE) {
      ask("Output file", "", &answer);
      if (!answer.empty()) {
        ofstream fs(answer.c_str());
        if (fs.is_open()) {
          fs << output.str();
          fs.close();
          cout << endl;
          cout << "\"" << answer << "\", " << output.str().size();
          cout << "bytes written" << endl;
        } else {
          cout << "ERROR, writing failed." << endl;
          NEXTSTATE(WRITE_TO_FILE);
        }
      } else  {
        cout << "=== OUTPUT START ====" << endl;
        cout << output.str();
        cout << "=== OUTPUT END   ====" << endl;
      }
    }
  }  // FSM

  return(0);
}  // main

/* EOF */

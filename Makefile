CXX       = c++
CXXFLAGS  = -ansi
#CXXFLAGS += -Wall -Wextra -Wconversion -pedantic
#CXXFLAGS += -DDEBUG -ggdb3
PREFIX    = /usr/local
sfx       = cc
name      = skeletor
args      =

all: build

build: $(name)

$(name): $(name).$(sfx)
	$(CXX) $(CXXFLAGS) -o $(name) $(name).$(sfx)

run:
	./$(name) $(args)

clean:
	rm -f $(name)

install: $(name)
	mkdir -p $(PREFIX)/bin
	cp $(name) $(PREFIX)/bin

uninstall:
	rm -f $(PREFIX)/bin/$(name)
